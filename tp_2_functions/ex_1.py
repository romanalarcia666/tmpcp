#!/usr/bin/env python3
# coding=UTF-8

# Exercise 1.1 write a function named printHello that prints "Hello"


# Exercise 1.2 write a function named printHelloTo(lastname, firstname) that prints "Hello", firstname, lastname




#*****************************************************************************#
#*****************************************************************************#
#***                                Checks                                 ***#
#*****************************************************************************#
#*****************************************************************************#
if __name__ == "__main__":
	print("**The expected output is :\nHello")
	print("->Your output :")
	try:
		printHello()
	except NameError:
		print("The function printHello() is not defined")

	print("\n**The expected output is :\nHello tata TOTO")
	print("->Your output :")
	try:
		printHelloTo("TOTO", "tata")
	except NameError:
		print("The function printHelloTo() is not defined")


