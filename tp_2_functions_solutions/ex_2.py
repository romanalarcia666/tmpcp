#!/usr/bin/env python3
# coding=UTF-8




# Exercise 2.1 Write functions mul(a, b), div(a, b), add(a, b) and sub(a, b) that returns
# the result of a*b, a/b, a+b and a-b
def mul(a, b):
	return a * b

div = lambda a, b : a / b

def add(a, b):
	return a + b

def sub(a, b):
	return a - b

# Exercise 2.2 functions divInfinityOnZeroTest(a, b) and divInfinityOnZeroException(a, b)
# function divInfinityOnZeroTest
	# use the function float("inf") (-float("inf")) to represent +∞ et -∞ ;
	# if b is equal to 0, you should return +∞ ou -∞ depending on the sign of a.
def divInfinityOnZeroTest(a, b):
	if b != 0:
		return a / b
	if a < 0:
		return -float('inf')
	if a > 0:
		return float('inf')
	# A little extra 0/0 is not a number
	return float('nan')

# function divInfinityOnZeroException
	# use the function float("inf") (-float("inf")) to represent +∞ et -∞ ;
	# do not check for (b == 0) before dividing, instead catch the exception
	# ZeroDivisionError then return +∞ ou -∞ depending on the sign of a.
def divInfinityOnZeroException(a, b):
	try:
		return a / b
	except ZeroDivisionError:
		if a < 0:
			return -float('inf')
		if a > 0:
			return float('inf')
		# A little extra 0/0 is not a number
		return float('nan')






#*****************************************************************************#
#*****************************************************************************#
#***                                Checks                                 ***#
#*****************************************************************************#
#*****************************************************************************#


print("Exercise 2.1")
try:
	print('  1 * 3 = ', mul(1, 3))
except NameError:
	print("The function mul() is not defined")
try:
	print("  1 / 3 = ", div(1, 3))
except NameError:
	print("The function div() is not defined")
try:
	print("  1 + 3 = ", add(1, 3))
except NameError:
	print("The function add() is not defined")
try:
	print("  1 - 3 = ", sub(1, 3))
except NameError:
	print("The function sub() is not defined")
try:
	print("  (1 + 3) * 11 + 1 - 3 = ", add(mul(add(1, 3), 11), sub(1, 3)))
except NameError:
	print("You miss one of the functions add, mul ou sub")

print("Exercise 2.2")
try:
	print("  +1 / 0 = ", divInfinityOnZeroTest(+1, 0))
	print("  -1 / 0 = ", divInfinityOnZeroTest(-1, 0))
except NameError:
	print("The function divInfinityOnZeroTest() is not defined")
try:
	print("  +1 / 0 = ", divInfinityOnZeroException(+1, 0))
	print("  -1 / 0 = ", divInfinityOnZeroException(-1, 0))
except NameError:
	print("The function divInfinityOnZeroException() is not defined")
